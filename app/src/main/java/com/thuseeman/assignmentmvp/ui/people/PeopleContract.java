package com.thuseeman.assignmentmvp.ui.people;

import com.thuseeman.assignmentmvp.ui.base.BasePresenter;
import com.thuseeman.assignmentmvp.ui.base.BaseView;
import com.thuseeman.assignmentmvp.data.Person;

import java.util.List;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public interface PeopleContract {
    interface View extends BaseView<Presenter>{

        void setTitle(int personType);

        void addPerson(int personType);

        void showProgress(boolean b);

        void updateUi();

        void updatePerson(Person person);
    }

    interface Presenter extends BasePresenter{

        void setPersonType(int personType);

        void setTitle();

        void loadData();

        void addPerson();

        List<Person> getPeople();

        void deletePerson(Person person);

        void updatePerson(Person person);

        int getType();
    }
}
