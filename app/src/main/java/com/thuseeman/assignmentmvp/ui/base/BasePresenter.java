package com.thuseeman.assignmentmvp.ui.base;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public interface BasePresenter {
    void unSubscribe();
}
