package com.thuseeman.assignmentmvp.ui.person_add;

import com.thuseeman.assignmentmvp.ui.base.BasePresenter;
import com.thuseeman.assignmentmvp.ui.base.BaseView;
import com.thuseeman.assignmentmvp.data.Person;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public interface PersonAddContract {
    interface View extends BaseView<Presenter> {

        void setTitle(int personType);

        String getName();

        void setError(int msg);

        void showToast(int msg);

        void finish();
    }

    interface Presenter extends BasePresenter {

        void setPersonType(int personType);

        void setTitle();

        void save();

        void setPerson(Person person);

        boolean isNew();

        Person getPerson();
    }
}
