package com.thuseeman.assignmentmvp.ui.person_add;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.thuseeman.assignmentmvp.R;
import com.thuseeman.assignmentmvp.data.Person;
import com.thuseeman.assignmentmvp.data.datasource.DataSource;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public class PersonAddPresenter implements PersonAddContract.Presenter {
    @NonNull
    private PersonAddContract.View mView;
    @Person.Type
    private int mPersonType;
    @NonNull
    private final DataSource mDataSource;
    private Person mPerson;

    public PersonAddPresenter(@NonNull PersonAddContract.View view, @NonNull DataSource dataSource){
        mView = view;
        mDataSource = dataSource;
        mView.setPresenter(this);
    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void setPersonType(int personType) {
        mPersonType = personType;
    }

    @Override
    public void setTitle() {
        mView.setTitle(mPersonType);
    }

    @Override
    public void save() {
        String name = mView.getName();
        if (TextUtils.isEmpty(name)){
            mView.setError(R.string.name_required);
            return;
        }

        //save to db
        if (mPerson == null) {
            Person person = new Person(0, name, mPersonType);
            mDataSource.savePerson(person);
        }else {
            mPerson.setName(name);
            mDataSource.updatePerson(mPerson);
        }

        mView.showToast(R.string.save_success);
        mView.finish();
    }

    @Override
    public void setPerson(Person person) {
        mPerson = person;
    }

    @Override
    public boolean isNew() {
        return mPerson == null;
    }

    @Override
    public Person getPerson() {
        return mPerson;
    }
}
