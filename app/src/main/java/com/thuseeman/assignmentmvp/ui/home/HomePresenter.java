package com.thuseeman.assignmentmvp.ui.home;

import android.support.annotation.NonNull;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public class HomePresenter implements HomeContract.Presenter {

    @NonNull
    private HomeContract.View mView;

    public HomePresenter(@NonNull HomeContract.View view){
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void openPeopleScreen(int personType) {
        mView.openPeopleScreen(personType);
    }
}
