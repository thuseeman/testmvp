package com.thuseeman.assignmentmvp.ui.home;

import com.thuseeman.assignmentmvp.ui.base.BasePresenter;
import com.thuseeman.assignmentmvp.ui.base.BaseView;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public interface HomeContract {
    interface View extends BaseView<Presenter> {

        void openPeopleScreen(int personType);
    }

    interface Presenter extends BasePresenter {

        void openPeopleScreen(int personType);
    }
}
