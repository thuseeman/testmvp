package com.thuseeman.assignmentmvp.ui.people;

import android.support.annotation.NonNull;

import com.thuseeman.assignmentmvp.data.Person;
import com.thuseeman.assignmentmvp.data.datasource.DataSource;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public class PeoplePresenter implements PeopleContract.Presenter {
    @NonNull
    private PeopleContract.View mView;
    @Person.Type
    private int mPersonType;
    private CompositeDisposable mSubscription;
    private DataSource mDataSource;
    private List<Person> mPeople = new ArrayList<>();

    public PeoplePresenter(@NonNull PeopleContract.View view, @NonNull DataSource dataSource) {
        mView = view;
        mDataSource = dataSource;
        mView.setPresenter(this);
        mSubscription = new CompositeDisposable();
    }

    @Override
    public void unSubscribe() {
        mSubscription.clear();
    }

    @Override
    public void setPersonType(@Person.Type int personType) {
        mPersonType = personType;
    }

    @Override
    public void setTitle() {
        mView.setTitle(mPersonType);
    }

    @Override
    public void loadData() {
        mSubscription.add(
                mDataSource.getPeople(mPersonType)
                        .observeOn(Schedulers.io())
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(c -> mView.showProgress(true))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(list -> {
                            mPeople.clear();
                            if (list != null) {
                                mPeople.addAll(list);
                            }
                            Timber.d("loadData size:%s", mPeople.size());
                            mView.showProgress(false);
                            mView.updateUi();
                        }, t -> {
                            t.printStackTrace();
                            mView.showProgress(false);
                        })
        );
    }

    @Override
    public void addPerson() {
        mView.addPerson(mPersonType);
    }

    @Override
    public List<Person> getPeople() {
        return mPeople;
    }

    @Override
    public void deletePerson(Person person) {
        mDataSource.deletePersonById(person.getId());
    }

    @Override
    public void updatePerson(Person person) {
        mView.updatePerson(person);
    }

    @Override
    public int getType() {
        return mPersonType;
    }
}
