package com.thuseeman.assignmentmvp.ui.people;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.thuseeman.assignmentmvp.R;
import com.thuseeman.assignmentmvp.data.Person;
import com.thuseeman.assignmentmvp.databinding.PersonItemBinding;
import com.thuseeman.assignmentmvp.event.ItemClickListener;

import java.util.List;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.ViewHolder> {
    private List<Person> mPeople;
    private ItemClickListener mClick;

    public PeopleAdapter(List<Person> people, ItemClickListener clickListener) {
        mPeople = people;
        mClick = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PersonItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.person_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Person person = mPeople.get(position);
        holder.binding.name.setText(person.getName());
        holder.binding.icon.setImageResource(person.getType() == Person.STUDENT ? R.drawable.ic_student : R.drawable.ic_teacher);
    }

    @Override
    public int getItemCount() {
        return mPeople.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        PersonItemBinding binding;

        public ViewHolder(PersonItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.delete.setOnClickListener(v -> mClick.onItemClick(v, getAdapterPosition()));
            binding.getRoot().setOnClickListener(v -> mClick.onItemClick(v, getAdapterPosition()));
        }
    }
}
