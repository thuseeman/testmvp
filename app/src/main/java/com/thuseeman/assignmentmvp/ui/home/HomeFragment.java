package com.thuseeman.assignmentmvp.ui.home;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thuseeman.assignmentmvp.ui.base.BaseFragment;
import com.thuseeman.assignmentmvp.Injector;
import com.thuseeman.assignmentmvp.R;
import com.thuseeman.assignmentmvp.data.Person;
import com.thuseeman.assignmentmvp.databinding.HomeFragmentBinding;
import com.thuseeman.assignmentmvp.ui.people.PeopleFragment;
import com.thuseeman.assignmentmvp.ui.people.PeoplePresenter;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public class HomeFragment extends BaseFragment implements HomeContract.View {
    private HomeFragmentBinding mBinding;
    private HomeContract.Presenter mPresenter;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        new HomePresenter(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false);

        mBinding.teacherLayout.setOnClickListener(v -> mPresenter.openPeopleScreen(Person.TEACHER));
        mBinding.studentLayout.setOnClickListener(v -> mPresenter.openPeopleScreen(Person.STUDENT));

        getActivity().setTitle(R.string.title_home);

        return mBinding.getRoot();
    }

    @Override
    public void setPresenter(HomeContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onStart() {
        super.onStart();
        //mPresenter.subscribe();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
    }

    @Override
    public void openPeopleScreen(int personType) {
        PeopleFragment peopleFragment = PeopleFragment.newInstance(personType);
        new PeoplePresenter(peopleFragment, Injector.getDatabaseSource(getContext()));
        openFragment(peopleFragment, true);
    }
}
