package com.thuseeman.assignmentmvp.ui.base;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.thuseeman.assignmentmvp.ui.MainActivity;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public class BaseFragment extends Fragment {

    public void openFragment(Fragment fragment, boolean addToBacStack) {
        getMainActivity().openFragment(fragment, addToBacStack);
    }

    @NonNull
    private MainActivity getMainActivity(){
        return (MainActivity) getActivity();
    }

    public void showToast(int msg){
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
