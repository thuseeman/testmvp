package com.thuseeman.assignmentmvp.ui.people;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thuseeman.assignmentmvp.ui.base.BaseFragment;
import com.thuseeman.assignmentmvp.Injector;
import com.thuseeman.assignmentmvp.R;
import com.thuseeman.assignmentmvp.data.Person;
import com.thuseeman.assignmentmvp.databinding.PeopleFragmentBinding;
import com.thuseeman.assignmentmvp.event.ItemClickListener;
import com.thuseeman.assignmentmvp.ui.person_add.PersonAddFragment;
import com.thuseeman.assignmentmvp.ui.person_add.PersonAddPresenter;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public class PeopleFragment extends BaseFragment implements PeopleContract.View, ItemClickListener {
    private PeopleFragmentBinding mBinding;
    private PeopleContract.Presenter mPresenter;

    public static PeopleFragment newInstance(@Person.Type int personType) {
        PeopleFragment peopleFragment = new PeopleFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("personType", personType);
        peopleFragment.setArguments(bundle);
        return peopleFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.people_fragment, container, false);

        mBinding.recyclerView.setHasFixedSize(true);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.recyclerView.setAdapter(new PeopleAdapter(mPresenter.getPeople(), this));

        int personType = getArguments().getInt("personType", Person.STUDENT);
        mPresenter.setPersonType(personType);
        mPresenter.setTitle();

        mBinding.add.setOnClickListener(v -> mPresenter.addPerson());

        return mBinding.getRoot();
    }

    @Override
    public void setPresenter(PeopleContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.loadData();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
    }

    @Override
    public void setTitle(int personType) {
        if (personType == Person.STUDENT) {
            getActivity().setTitle(getString(R.string.title_students));
        } else {
            getActivity().setTitle(getString(R.string.title_teachers));
        }
    }

    @Override
    public void addPerson(int personType) {
        PersonAddFragment personAddFragment = PersonAddFragment.newInstance(personType);
        new PersonAddPresenter(personAddFragment, Injector.getDatabaseSource(getActivity().getApplicationContext()));
        openFragment(personAddFragment, true);
    }

    @Override
    public void showProgress(boolean b) {
        mBinding.progressBar.setVisibility(b ? View.VISIBLE : View.GONE);
    }

    @Override
    public void updateUi() {
        if (mPresenter.getPeople().isEmpty()) {
            if (mPresenter.getType() == Person.STUDENT) {
                mBinding.empty.setText(R.string.add_students);
            } else {
                mBinding.empty.setText(R.string.add_teachers);
            }
        } else {
            mBinding.empty.setText(null);
        }
        mBinding.recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void updatePerson(Person person) {
        PersonAddFragment personAddFragment = PersonAddFragment.newInstance(person);
        new PersonAddPresenter(personAddFragment, Injector.getDatabaseSource(getActivity().getApplicationContext()));
        openFragment(personAddFragment, true);
    }

    @Override
    public void onItemClick(View view, int position) {
        Person person = mPresenter.getPeople().get(position);
        switch (view.getId()) {
            case R.id.delete:
                new AlertDialog.Builder(getContext())
                        .setTitle("Delete")
                        .setMessage("Are you sure you want to delete '" + person.getName() + "'")
                        .setNegativeButton("No", null)
                        .setPositiveButton("Yes", (dialogInterface, i) -> {
                            mPresenter.deletePerson(person);
                            dialogInterface.dismiss();
                        })
                        .show();
                break;
            default:
                mPresenter.updatePerson(person);
                break;
        }
    }
}
