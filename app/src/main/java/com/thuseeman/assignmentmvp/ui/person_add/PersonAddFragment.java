package com.thuseeman.assignmentmvp.ui.person_add;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.thuseeman.assignmentmvp.ui.base.BaseFragment;
import com.thuseeman.assignmentmvp.R;
import com.thuseeman.assignmentmvp.data.Person;
import com.thuseeman.assignmentmvp.databinding.PersonAddFragmentBinding;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public class PersonAddFragment extends BaseFragment implements PersonAddContract.View {
    private PersonAddFragmentBinding mBinding;
    private PersonAddContract.Presenter mPresenter;

    public static PersonAddFragment newInstance(@Person.Type int personType) {
        PersonAddFragment personAddFragment = new PersonAddFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("personType", personType);
        personAddFragment.setArguments(bundle);
        return personAddFragment;
    }

    public static PersonAddFragment newInstance(Person person) {
        PersonAddFragment personAddFragment = new PersonAddFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("person", person);
        personAddFragment.setArguments(bundle);
        return personAddFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.person_add_fragment, container, false);

        mPresenter.setPerson(getArguments().getParcelable("person"));
        mPresenter.setPersonType(getArguments().getInt("personType", Person.STUDENT));
        mPresenter.setTitle();

        if (mPresenter.isNew()){
            mBinding.save.setText(R.string.save);
        }else {
            mBinding.name.setText(mPresenter.getPerson().getName());
            mBinding.save.setText(R.string.update);
        }

        mBinding.save.setOnClickListener(v -> mPresenter.save());
        mBinding.name.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_DONE) {
                mPresenter.save();
            }
            return false;
        });

        return mBinding.getRoot();
    }

    @Override
    public void setPresenter(PersonAddContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onStart() {
        super.onStart();
        //mPresenter.subscribe();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
    }

    @Override
    public void setTitle(int personType) {
        if (mPresenter.isNew()){
            if (personType == Person.STUDENT) {
                getActivity().setTitle(getString(R.string.title_add_student));
            } else {
                getActivity().setTitle(getString(R.string.title_add_teacher));
            }
        }else {
            if (personType == Person.STUDENT) {
                getActivity().setTitle(getString(R.string.title_update_student));
            } else {
                getActivity().setTitle(getString(R.string.title_update_teacher));
            }
        }

    }

    @Override
    public String getName() {
        return mBinding.name.getText().toString();
    }

    @Override
    public void setError(int msg) {
        mBinding.name.setError(getString(msg));
    }

    @Override
    public void finish() {
        getActivity().onBackPressed();
    }


}
