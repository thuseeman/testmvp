package com.thuseeman.assignmentmvp;

import android.content.Context;

import com.thuseeman.assignmentmvp.data.datasource.DataSource;
import com.thuseeman.assignmentmvp.data.datasource.Repository;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public class Injector {
    public static DataSource getDatabaseSource(Context context){
        return new Repository(context);
    }
}
