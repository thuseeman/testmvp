package com.thuseeman.assignmentmvp.event;

import android.view.View;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public interface ItemClickListener {
    void onItemClick(View view, int position);
}
