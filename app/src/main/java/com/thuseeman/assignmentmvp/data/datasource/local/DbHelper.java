package com.thuseeman.assignmentmvp.data.datasource.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public class DbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "assignment.db";
    private static final int DATABASE_VERSION = 6;

    private static final String TEXT_TYPE = " TEXT";

    private static final String INTEGER_TYPE = " INTEGER";

    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + PeopleDbContract.PeopleEntry.TABLE_NAME + " (" +
                    PeopleDbContract.PeopleEntry._ID + INTEGER_TYPE +" PRIMARY KEY " + COMMA_SEP +
                    PeopleDbContract.PeopleEntry.NAME + TEXT_TYPE + COMMA_SEP +
                    PeopleDbContract.PeopleEntry.TYPE + INTEGER_TYPE +
                    " )";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
