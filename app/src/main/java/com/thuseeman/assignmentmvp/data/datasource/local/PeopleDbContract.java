package com.thuseeman.assignmentmvp.data.datasource.local;

import android.provider.BaseColumns;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public interface PeopleDbContract {

    interface PeopleEntry extends BaseColumns {
        String TABLE_NAME = "people";
        String NAME = "name";
        String TYPE = "type";
    }
}
