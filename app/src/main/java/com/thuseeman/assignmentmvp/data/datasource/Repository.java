package com.thuseeman.assignmentmvp.data.datasource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.squareup.sqlbrite2.BriteDatabase;
import com.squareup.sqlbrite2.SqlBrite;
import com.thuseeman.assignmentmvp.data.Person;
import com.thuseeman.assignmentmvp.data.datasource.local.DbHelper;
import com.thuseeman.assignmentmvp.data.datasource.local.PeopleDbContract;

import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public class Repository implements DataSource {
    @NonNull
    private final BriteDatabase mDatabaseHelper;

    //cursor mapper with person object
    private Function<Cursor, Person> mPeopleMapperFunction = c -> extractPeople(c);

    public Repository(Context context) {
        SqlBrite sqlBrite = new SqlBrite.Builder().build();
        DbHelper dbHelper = new DbHelper(context.getApplicationContext());
        mDatabaseHelper = sqlBrite.wrapDatabaseHelper(dbHelper, Schedulers.io());
    }

    @Override
    public Flowable<List<Person>> getPeople(int personType) {
        String sql = String.format("SELECT * FROM %s WHERE %s = ?"
                , PeopleDbContract.PeopleEntry.TABLE_NAME
                , PeopleDbContract.PeopleEntry.TYPE);
        return mDatabaseHelper.createQuery(PeopleDbContract.PeopleEntry.TABLE_NAME, sql
                , new String[]{personType + ""}).mapToList(mPeopleMapperFunction)
                .toFlowable(BackpressureStrategy.BUFFER);
    }

    @Override
    public void savePerson(Person person) {
        ContentValues values = new ContentValues();
        values.put(PeopleDbContract.PeopleEntry.NAME, person.getName());
        values.put(PeopleDbContract.PeopleEntry.TYPE, person.getType());
        mDatabaseHelper.insert(PeopleDbContract.PeopleEntry.TABLE_NAME, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    @Override
    public void deletePersonById(int id) {
        mDatabaseHelper.delete(PeopleDbContract.PeopleEntry.TABLE_NAME,
                PeopleDbContract.PeopleEntry._ID + "=?", String.valueOf(id));
    }

    @Override
    public void updatePerson(Person person) {
        ContentValues values = new ContentValues();
        values.put(PeopleDbContract.PeopleEntry.NAME, person.getName());
        mDatabaseHelper.update(PeopleDbContract.PeopleEntry.TABLE_NAME, values,
                PeopleDbContract.PeopleEntry._ID + "=?", String.valueOf(person.getId()));
    }

    private Person extractPeople(Cursor c) {
        int id = c.getInt(c.getColumnIndexOrThrow(PeopleDbContract.PeopleEntry._ID));
        String name = c.getString(c.getColumnIndexOrThrow(PeopleDbContract.PeopleEntry.NAME));
        int type = c.getInt(c.getColumnIndexOrThrow(PeopleDbContract.PeopleEntry.TYPE));
        Person person = new Person(id, name, type);
        return person;
    }
}
