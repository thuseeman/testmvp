package com.thuseeman.assignmentmvp.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public class Person implements Parcelable {
    public static final int STUDENT = 1;
    public static final int TEACHER = 2;

    private String name;
    private int type;
    private int id;

    public Person(int id, String name, int type) {
        this.name = name;
        this.id = id;
        this.type = type;
    }

    protected Person(Parcel in) {
        name = in.readString();
        type = in.readInt();
        id = in.readInt();
    }

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Type
    public int getType() {
        return type;
    }

    public void setType(@Type int type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeInt(type);
        parcel.writeInt(id);
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({STUDENT, TEACHER})
    public @interface Type {
    }
}
