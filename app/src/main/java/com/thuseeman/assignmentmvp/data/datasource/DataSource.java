package com.thuseeman.assignmentmvp.data.datasource;

import com.thuseeman.assignmentmvp.data.Person;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Thuseeman on 10/14/2017.
 */

public interface DataSource {
    Flowable<List<Person>> getPeople(int personType);

    void savePerson(Person person);

    void deletePersonById(int id);

    void updatePerson(Person person);
}
